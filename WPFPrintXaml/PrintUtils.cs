﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;

namespace WPFPrintXaml
{
    static class PrintUtils
    {
        public static void Print(UserControl printData)
        {
            var document = CreateDocument(printData);
            WriteDocument(document);
        }

        private static FixedDocument CreateDocument(UserControl printData)
        {
            var page = new FixedPage();
            page.Children.Add(printData);

            var content = new PageContent();
            content.Child = page;

            var document = new FixedDocument();
            document.Pages.Add(content);

            return document;
        }

        private static void WriteDocument(FixedDocument document)
        {
            var server = new LocalPrintServer();
            var queue = server.DefaultPrintQueue;

            var ticket = queue.DefaultPrintTicket;
            ticket.PageMediaSize = new PageMediaSize(PageMediaSizeName.ISOA4);
            ticket.PageOrientation = PageOrientation.Portrait;

            var writer = PrintQueue.CreateXpsDocumentWriter(queue);
            writer.Write(document, ticket);
        }
    }
}
